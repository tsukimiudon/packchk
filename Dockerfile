FROM debian:stable-slim AS wget

ARG cmsis_ver="5.5.1"

RUN set -eux; \
      apt update \
      && apt install -y --no-install-recommends \
        wget \
        unzip \
      && wget --no-check-certificate "https://github.com/ARM-software/CMSIS_5/releases/download/${cmsis_ver}/ARM.CMSIS.${cmsis_ver}.pack" \
      && unzip -j -d /root ARM.CMSIS.${cmsis_ver}.pack ARM.CMSIS.pdsc CMSIS/Utilities/Linux-gcc-4.8.3/PackChk \
      && chmod +x /root/PackChk \
      && apt remove -y --purge \
        wget \
        unzip \
      && apt autoremove -y \
      && apt clean \
      && rm -rf /ver/lib/apt/list/*


FROM debian:stable-slim

COPY --from=wget /root/*.pdsc /root/PackChk /root/
ENV PATH $PATH:/root
WORKDIR /root
ENTRYPOINT ["/root/PackChk", "-i", "/root/ARM.CMSIS.pdsc"]
